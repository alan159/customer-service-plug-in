import axios from "./config";
export function getUser(page:number, size:number) {
  return axios({
    url: "/resources-action/user",
    method: "get",
    params: {
      page:page-1,
      size,
    },
  });
}
