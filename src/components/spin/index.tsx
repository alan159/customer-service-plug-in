import { Spin } from "antd";
const sspin = () => {
  return (
    // eslint-disable-next-line
    <div
      style={{
        position: "absolute",
        width: "100%",
        height: "100%",
        top: "0",
        left: "0",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Spin size="large" />
    </div>
  );
};
export default sspin;
//TODO 怎么防止闪烁
