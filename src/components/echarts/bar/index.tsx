import * as echarts from "echarts/core";
import { useRef, useEffect } from "react";
import {
  TitleComponent,
  TooltipComponent,
  GridComponent,
  LegendComponent,
} from "echarts/components";
import { BarChart } from "echarts/charts";
import { CanvasRenderer } from "echarts/renderers";
import  propType  from "../type";
import { echartsResize } from "../untils/resize"
echarts.use([
  TitleComponent,
  TooltipComponent,
  GridComponent,
  LegendComponent,
  BarChart,
  CanvasRenderer,
]);

const ECharts = (props: propType) => {
  const echartsRef: any = useRef();

  const option = {
    title: {
      text: props.title,
    },
    tooltip: {},
    xAxis: {
      data: props.xData,
    },
    yAxis: {},
    series: [
      {
        name: "访问量",
        type: "bar",
        data: props.seriesData,
      },
    ],
  };
  useEffect(() => {
    var myChart = echarts.init(echartsRef.current);
    option && myChart.setOption(option);
    echartsResize(myChart)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div
      ref={echartsRef}
      style={{
        width: '100%',
        height: '100%',
      }}
    ></div>
  );
};
export default ECharts;
