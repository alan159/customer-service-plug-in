import { useState, useEffect } from "react";
import { Table } from "antd";
import { getUser } from "../../../../api/mock";
const columns = [
  {
    title: "名字",
    dataIndex: "username",
    // sorter: true,
    width: "20%",
  },
  {
    title: "手机号",
    dataIndex: "phone_number",
    width: "20%",
  },
  {
    title: "权限",
    dataIndex: "authority",
  },
];

const Tables = () => {
  const [data, setdata] = useState([]);
  const [pagination, setpagination] = useState({
    current: 1,
    pageSize: 10,
    total: 10,
  });
  const [loading, setloading] = useState(false);
  const handleTableChange = (pagination: any, filters: any, sorter: any) => {
    userList(pagination.current,pagination.pageSize)
  };
  const userList = (current: number, pageSize: number) => {
    getUser(current, pageSize)
      .then((result) => {
        setloading(false);
        let { page } = result.data;
        console.log(page);
        setdata(result.data._embedded.user_infoes);
        setpagination({
          current: page.number + 1,
          pageSize: page.size,
          total: page.total_elements,
        });
      })
      .catch((err) => {});
  };
  useEffect(() => {
    setloading(true);
    console.log(pagination);
    userList(pagination.current, pagination.pageSize);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Table
      columns={columns}
      rowKey={(record: any) => record.id}
      dataSource={data}
      pagination={pagination}
      loading={loading}
      onChange={handleTableChange}
    />
  );
};
export default Tables;
