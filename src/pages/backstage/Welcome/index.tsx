// import { Button } from "antd";
import BarECharts from "../../../components/echarts/bar";
import "./index.less";
const Welcome = () => {
  const Edata = {
    title:"访问量",
    xData:["1月","2月","3月","4月","5月","6月"],
    seriesData:[20,30,70,50,60,20],
  }
  return (
    <div style={{height:400}}>
    <BarECharts {...Edata}/>
    </div>
  );
};
export default Welcome;
