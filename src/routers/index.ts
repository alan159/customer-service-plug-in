import RouterType from "./routerType";
import { lazy } from "react";
const Reception: RouterType={
  path: "/",
  component: lazy(() => import("../pages/home")),
  exact: true,
}
const LoginRoutes: RouterType = {
  path: "/login",
  component: lazy(() => import("../pages/login")),
  exact: true,
};
const BackstageRoutes: RouterType = {
  path: "/admin",
  component: lazy(() => import("../layout/backstage")),
  exact: false,
  routes: [
    {
      path: "/admin/welcome",
      component: lazy(() => import("../pages/backstage/Welcome")),
      exact: true,
    },
    {
      path: "/admin/account/center",
      component: lazy(() => import("../pages/backstage/Account/center")),
      exact: true,
    },
    {
      path: "/admin/account/settings",
      component: lazy(() => import("../pages/backstage/Account/settings")),
      exact: true,
    },
    {
      path: "/admin/manage/user",
      component: lazy(() => import("../pages/backstage/Person/User")),
      exact: true,
    },
    {
      path: "/admin/manage/admin",
      component: lazy(() => import("../pages/backstage/Person/Admin")),
      exact: true,
    },
  ],
};
// {
//   path: "/admin/account",
//   component: Sider,
//   exact: false,
//   routes: [
//     {
//       path: "/admin/account/settings",
//       component: AccountSettings,
//       exact: true,
//     },
//     {
//       path: "/account/center",
//       component: AccountCenter,
//       exact: true,
//     },
//   ],
// },

const Routers = [LoginRoutes, BackstageRoutes,Reception];
export { Routers };
