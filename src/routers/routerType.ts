export default interface RouterType {
  path: string;
  component: React.LazyExoticComponent<any>;
  //子路由
  routes?: Array<RouterType>;
  notExect?: boolean;
  exact?: boolean;
  // 302 跳转
  redirect?: any;
  // 路由信息
  meta?: {
    primaryTitle:string,
    secondaryTitle?:string
  };
  // 是否校验权限, false 为不校验, 不存在该属性或者为true 为校验, 子路由会继承父路由的 auth 属性
  auth?: string;
}
