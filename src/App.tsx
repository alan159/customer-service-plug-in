import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import Login from "./pages/login";
import { Routers } from "./routers";
function App(props:any) {
  return (
    <Switch>
      {Routers.map((routes) => (
        <Route
          key={routes.path}
          path={routes.path}
          render={(props) => (
            <routes.component {...props} routes={routes.routes} />
          )}
        ></Route>
      ))}
      <Route path="/" component={Login} exact></Route>
      <Redirect to="/404"></Redirect>
    </Switch>
  );
}

export default withRouter(App);
