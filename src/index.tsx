import { Suspense } from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { BrowserRouter} from 'react-router-dom'
import  Spin  from './components/spin';

ReactDOM.render(
  <BrowserRouter>
    {/* 使用了路由懒加载，所以需要使用<Suspense>包起来 */}
    <Suspense fallback={<Spin/>}>
        <App />
    </Suspense>
  </BrowserRouter>,
  document.getElementById('root')
)
 