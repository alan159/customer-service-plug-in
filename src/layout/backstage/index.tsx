import { Layout } from "antd";
import { useState } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import MenuElem from '../../components/menu'
import Header from "../../components/header";
import "./index.less";

const { Content, Footer, Sider } = Layout;

function SiderD({routes}:any) {
  const [collapsed, setCollapsed] = useState(false);
  const onCollapse = (collapsed: boolean) => {
    setCollapsed(collapsed);
  };
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider collapsible collapsed={collapsed} onCollapse={onCollapse} theme='light' >
        <div className="logo" ><img src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" alt="logo" /></div>
        <MenuElem/>
      </Sider>
      <Layout className="site-layout">
        <Header/>
        <Content style={{ margin: "16px" }}>
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 360 }}
          >
            <Switch>
              {routes.map((routes: any) => (
                <Route
                  key={routes.path}
                  path={routes.path}
                  render={(props) => (
                    <routes.component {...props} />
                  )}
                />
              ))}
              <Redirect to='/admin/welcome' path="/admin"></Redirect>
              <Redirect to='404'></Redirect>
            </Switch>
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Future customer service ©2021 Created by Alan
        </Footer>
      </Layout>
    </Layout>
  );
}
export default SiderD;
